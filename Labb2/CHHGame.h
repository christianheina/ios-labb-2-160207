//
//  CHHGame.h
//  Labb2
//
//  Created by Christian on 2016-02-02.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CHHGame : NSObject
@property (nonatomic, readonly) NSInteger correctAnswers;
@property (nonatomic,readonly) NSInteger wrongAnswers;

-(NSDictionary*)randomlySelectUniqueQuestion;
-(BOOL)checkAnswerAndAddPoints:(NSString*)answer;
-(NSArray*)shuffleArray:(NSArray*)array;

@end
