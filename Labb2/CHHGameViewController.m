//
//  CHHGameViewController.m
//  Labb2
//
//  Created by Christian on 2016-02-02.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import "CHHGameViewController.h"
#include "CHHGame.h"
#include "CHHApplicationInfo.h"

@interface CHHGameViewController ()
@property (weak, nonatomic) IBOutlet UITextView *questionTextView;
@property (weak, nonatomic) IBOutlet UIButton *answerButton1;
@property (weak, nonatomic) IBOutlet UIButton *answerButton2;
@property (weak, nonatomic) IBOutlet UIButton *answerButton3;
@property (weak, nonatomic) IBOutlet UIButton *answerButton4;

@property (nonatomic) CHHGame *game;
@property (nonatomic) NSDictionary *question;
@property (nonatomic) NSArray *buttons;
@property (nonatomic) NSInteger questionsAnswered;

@end

const NSInteger nRounds = 5;

@implementation CHHGameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.buttons = @[self.answerButton1, self.answerButton2, self.answerButton3, self.answerButton4];
    self.game = [[CHHGame alloc] init];
    [self populateViewWithNextQuestion];
}

- (IBAction)buttonClicked:(UIButton *)sender {
    [self disableButtons];
    if([self.game checkAnswerAndAddPoints:sender.currentTitle]){
        [sender setBackgroundColor:[UIColor greenColor]];
    }else{
        [sender setBackgroundColor:[UIColor redColor]];
        [NSTimer scheduledTimerWithTimeInterval:.3 target:self selector:@selector(showRightAnswer) userInfo:nil repeats:NO];
    }
    [NSTimer scheduledTimerWithTimeInterval:.6 target:self selector:@selector(setUpNextQuestionOrEndGame) userInfo:nil repeats:NO];
}

-(void)populateViewWithNextQuestion{
    self.buttons = [self.game shuffleArray:self.buttons];
    self.question = [self.game randomlySelectUniqueQuestion];
    self.questionTextView.text = self.question[@"question"];
    [self.buttons[0] setTitle:self.question[@"correctAnswer"] forState:UIControlStateNormal];
    [self.buttons[1] setTitle:self.question[@"wrongAnswer1"] forState:UIControlStateNormal];
    [self.buttons[2] setTitle:self.question[@"wrongAnswer2"] forState:UIControlStateNormal];
    [self.buttons[3] setTitle:self.question[@"wrongAnswer3"] forState:UIControlStateNormal];
}

- (void)setUpNextQuestionOrEndGame{
    self.questionsAnswered++;
    if(self.questionsAnswered >= nRounds){
        [self endGame];
    }
    [self setButtonsToDefault];
    [self populateViewWithNextQuestion];
}

-(void)disableButtons{
    for (UIButton *b in self.buttons) {
        [b setEnabled:NO];
    }
}

-(void)setButtonsToDefault{
    for (UIButton *b in self.buttons){
        [b setBackgroundColor:[UIColor lightGrayColor]];
        [b setEnabled:YES];
    }
}

-(void)showRightAnswer{
    for (UIButton *b in self.buttons){
        if([b.currentTitle isEqual:self.question[@"correctAnswer"]]){
            [b setBackgroundColor:[UIColor greenColor]];
        }
    }
}

-(void)endGame{
    [CHHApplicationInfo setCorrectAnswer:self.game.correctAnswers];
    [CHHApplicationInfo setWrongAnswer:self.game.wrongAnswers];
    UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *controller = [board instantiateViewControllerWithIdentifier:@"resultView"];
    [self presentViewController:controller animated:YES completion:nil];
}

@end
