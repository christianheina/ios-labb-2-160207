//
//  CHHApplicationInfo.h
//  Labb2
//
//  Created by Christian on 2016-02-02.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CHHApplicationInfo : NSObject
+(NSInteger) getCorrectAnswers;
+(void) setCorrectAnswer:(NSInteger)correct;
+(NSInteger) getWrongAnswers;
+(void) setWrongAnswer:(NSInteger)wrongs;

@end
