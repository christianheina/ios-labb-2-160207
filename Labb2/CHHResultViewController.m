//
//  CHHResultViewController.m
//  Labb2
//
//  Created by Christian on 2016-02-02.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import "CHHResultViewController.h"
#include "CHHApplicationInfo.h"

@interface CHHResultViewController()
@property (weak, nonatomic) IBOutlet UILabel *correctAnswersLabel;
@property (weak, nonatomic) IBOutlet UILabel *wrongAnswersLabel;

@end

@implementation CHHResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSInteger correct = [CHHApplicationInfo getCorrectAnswers];
    NSInteger wrong = [CHHApplicationInfo getWrongAnswers];
    [self.correctAnswersLabel setText:[NSString stringWithFormat:@"%ld", (long)correct]];
    [self.wrongAnswersLabel setText:[NSString stringWithFormat:@"%ld", (long)wrong]];
}

@end
