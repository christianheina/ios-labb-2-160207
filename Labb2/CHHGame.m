//
//  CHHGame.m
//  Labb2
//
//  Created by Christian on 2016-02-02.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import "CHHGame.h"

@interface CHHGame ()
@property (nonatomic) NSDictionary *question;
@property (nonatomic) NSMutableSet *usedQuestions;
@property (nonatomic, readwrite) NSInteger correctAnswers;
@property (nonatomic, readwrite) NSInteger wrongAnswers;
@property (nonatomic) NSArray *allQuestions;

@end

@implementation CHHGame

-(instancetype)init{
    self = [super init];
    self.usedQuestions = [[NSMutableSet alloc] init];
    [self fillQuestionArray];
    return self;
}

-(NSDictionary*)randomlySelectUniqueQuestion{
    NSInteger choice = 0;
    while ((self.correctAnswers + self.wrongAnswers) < self.allQuestions.count) {
        choice = arc4random()%self.allQuestions.count;
        NSNumber *temp = [NSNumber numberWithInteger:choice];
        if (![self.usedQuestions containsObject:temp]){
            [self.usedQuestions addObject:temp];
            break;
        }
    }
    self.question = self.allQuestions[choice];
    return self.question;
}

-(BOOL)checkAnswerAndAddPoints:(NSString*)answer{
    if([answer isEqualToString:self.question[@"correctAnswer"]]){
        self.correctAnswers++;
        return YES;
    }
    self.wrongAnswers++;
    return NO;
}

-(NSArray*) shuffleArray:(NSArray*)array{
    NSMutableArray *mutableArray = [array mutableCopy];
    for(NSUInteger i = 0; i < mutableArray.count; i++){
        NSUInteger rand = arc4random()%mutableArray.count;
        [mutableArray exchangeObjectAtIndex:rand withObjectAtIndex:i];
    }
    return mutableArray;
}

-(void)fillQuestionArray{
    self.allQuestions = @[
                          @{@"question": @"Vilken är den romerska siffran för 100?",
                            @"correctAnswer": @"C",
                            @"wrongAnswer1": @"D",
                            @"wrongAnswer2": @"M",
                            @"wrongAnswer3": @"X"},
                          
                          @{@"question": @"Ekonomi - Vad kallas en ekonomisk förenings högsta beslutande organ?",
                            @"correctAnswer": @"Föreningsstämma",
                            @"wrongAnswer1": @"Bolagsstämma",
                            @"wrongAnswer2": @"Styrelse",
                            @"wrongAnswer3": @"Kommission"},
                          
                          @{@"question": @"Ekonomi - Vilket av nedanstående alternativ är ett annat ord för realkapital?",
                            @"correctAnswer": @"Investeringar",
                            @"wrongAnswer1": @"Naturresurser",
                            @"wrongAnswer2": @"Arbetskraft",
                            @"wrongAnswer3": @"Råvaror "},
                          
                          @{@"question": @"Vilken dag under påsk, enligt nya testamentet, skedde Jesu uppståndelse?",
                            @"correctAnswer": @"Påskdagen",
                            @"wrongAnswer1": @"Påskafton",
                            @"wrongAnswer2": @"Långfredag",
                            @"wrongAnswer3": @"Skärtorsdag"},
                          
                          @{@"question": @"I vilken stad ligger Spanska ridskolan?",
                            @"correctAnswer": @"Wien",
                            @"wrongAnswer1": @"Madrid",
                            @"wrongAnswer2": @"Rom",
                            @"wrongAnswer3": @"Barcelona"},
                          
                          @{@"question": @"Många svenska städer har slogans tex \"Umeå - björkarnas stad\". Vilken stad i Sverige förknippas med smeknamnet \"lilla London\"?",
                            @"correctAnswer": @"Göteborg",
                            @"wrongAnswer1": @"Halmstad",
                            @"wrongAnswer2": @"Uppsala",
                            @"wrongAnswer3": @"Stockholm"},
                          
                          @{@"question": @"För vilket företag låtsas George i Seinfeld arbeta åt för att lura arbetsförmedlingen?",
                            @"correctAnswer": @"Vandelay Industries",
                            @"wrongAnswer1": @"New York Yankees",
                            @"wrongAnswer2": @"The Coca-cola company",
                            @"wrongAnswer3": @"Kramerica Industries"},
                          
                          @{@"question": @"Vad är batist?",
                            @"correctAnswer": @"Ett tunt tyg",
                            @"wrongAnswer1": @"En fladdermusjägare",
                            @"wrongAnswer2": @"Ett mikroskopiskt batteri",
                            @"wrongAnswer3": @"Ett instrument"},
                          
                          @{@"question": @"Vilket är världens mest glesbefolkade land?",
                            @"correctAnswer": @"Mongoliet",
                            @"wrongAnswer1": @"Island",
                            @"wrongAnswer2": @"Australien",
                            @"wrongAnswer3": @"Ryssland"},
                          
                          @{@"question": @"Ungefär hur mycket av vår atmosfär är syre?",
                            @"correctAnswer": @"20%",
                            @"wrongAnswer1": @"10%",
                            @"wrongAnswer2": @"30%",
                            @"wrongAnswer3": @"40%"},
    ];
}

@end
