//
//  CHHApplicationInfo.m
//  Labb2
//
//  Created by Christian on 2016-02-02.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import "CHHApplicationInfo.h"

static NSInteger correctAnswers;
static NSInteger wrongAnswers;

@implementation CHHApplicationInfo

+(NSInteger) getCorrectAnswers{
    return correctAnswers;
}

+(void) setCorrectAnswer:(NSInteger)corrects{
    correctAnswers = corrects;
}

+(NSInteger) getWrongAnswers{
    return wrongAnswers;
}

+(void) setWrongAnswer:(NSInteger)wrongs{
    wrongAnswers = wrongs;
}

@end
